from dash import Dash, dcc, html, Input, Output, ctx, dash_table
import pandas as pd
import plotly.express as px

# read from csv
df_school = pd.read_csv("school.csv")
df_alumni = pd.read_csv("alumni.csv")

# attach school to alumni
df_alumni = pd.merge(df_alumni, df_school, on='school')

# set id which is needed by filter
df_alumni['id'] = df_alumni['name']
df_alumni.set_index('id', inplace=True, drop=False)

# count how many student in each school
def school_from_alumni(alumni):
    location_counts = alumni.groupby(
        'school').size().reset_index(name='counts')

    return pd.merge(df_school, location_counts, on='school')


app = Dash()
app.title = "Global Alumni Map"
app.layout = html.Div(
    [
        # html.Header("Global Alumni Map",style={"font-size": "30px", "textAlign": "center"}),

        # where map os going to be display
        dcc.Graph(id="map"),

        # where table os going to be display
        dash_table.DataTable(
            data=df_alumni.to_dict('records'),
            id="table",
            page_size=9,
            # set id for columns such that filter could work
            columns=[{"name": i, 'id': i} for i in df_alumni.columns],
            sort_action="native",
            filter_action="native",
            #hide unnecessary columns
            style_cell_conditional=[
                {'if': {'column_id': column},
                 'display': 'none'}
                for column in ['lat', 'lon', 'id']
            ]
        )
    ],
)


@app.callback(
    Output('map', 'figure'),
    Input('table', 'derived_virtual_row_ids'),
    Input('table', 'active_cell'),
)
def update_graphs(row_ids, active_cell):
    # this is mainly copyed from demo
    print(row_ids, active_cell)
    if row_ids is None:
        df = df_alumni
        row_ids = df_alumni['id']
    else:
        df = df_alumni.loc[row_ids]

    # todo: highlight the active cell
    active_row_id = active_cell['row_id'] if active_cell else None
    print(row_ids, active_row_id)
    df = school_from_alumni(df)

    fig = px.scatter_mapbox(
        data_frame=df,
        lat="lat",
        lon="lon",
        size="counts",
        hover_name="school",
        zoom=1,
        mapbox_style="open-street-map",
    )
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})

    return fig


if __name__ == "__main__":
    app.run_server(debug=True)
